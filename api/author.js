import {requester} from "../common/requester";

class AuthorApi {
    async get(id, forEdit= false) {
        return await requester.get(`author/${id}?forEdit=${forEdit}`)
    }

    async getAll() {
        return await requester.get('author')
    }

    async create (data) {
        return await requester.post('author/create', data)
    }

    async delete (id) {
        return await requester.delete(`author/${id}`)
    }

    async edit (id, data) {
        return await requester.put(`author/${id}`, data)
    }
}

export const authorApi = new AuthorApi()