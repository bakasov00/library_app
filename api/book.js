import {requester} from "../common/requester";

class BookApi {
    async get(id, forEdit=false) {
        return await requester.get(`book/${id}?forEdit=${forEdit}`)
    }

    async getAll() {
        return await requester.get('book')
    }

    async create (data) {
        return await requester.post('book/create', data)
    }

    async delete (id) {
        return await requester.delete(`book/${id}`);
    }

    async edit (id, data) {
        return await requester.put(`book/${id}`, data);
    }
}

export const bookApi = new BookApi()