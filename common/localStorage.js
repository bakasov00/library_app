class LocalStorage {
    get(key) {
        const data = localStorage.getItem(key)
        return JSON.parse(data)
    }

    set(key, value) {
        const str = JSON.stringify(value)
        return localStorage.setItem(key, str)
    }

    clear() {
        localStorage.clear()
    }
}

export const storage = new LocalStorage()