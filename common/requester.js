import {API_URL} from "./constants";
import {baseStore} from "../store/BaseStore";

const req = async (to, method = 'GET', body = null) => {
    try {
        const url = `${API_URL}/${to}`
        const headers = {}
        if (body) {
            headers['Content-Type'] = 'application/json'
        }
        baseStore.loading = true
        const response = await fetch(url, {method: method, body: body ? JSON.stringify(body) : null, headers: headers})
        return await response.json()
    } catch (err) {
        err.message = err.message ? err.message : 'Что то пошло не так'
        baseStore.notify(err.message, 'error')
        return err
    } finally {
        baseStore.loading = false
    }
}

class Requester  {
    async get (to) {
        return await req(to, "GET")
    }

    async post (to, body) {
        return await req(to, "POST", body)
    }

    async delete (to) {
        return await req(to, "DELETE")
    }

    async put (to, data) {
        return await req(to, "PUT", data)
    }
}

export const requester = new Requester()
