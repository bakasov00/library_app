import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

export default function DataTable({headTable, children}) {
    return (
        <TableContainer>
            <Table sx={{minWidth: 650}}>
                <TableHead>
                    <TableRow>
                        {headTable && headTable.map(renderTableCell)}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {children}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

const renderTableCell = (item, idx) =>
    <TableCell style={{ fontWeight: 900 }} key={idx} align={idx === 0 ? "left" : "right"}>
        {item}
    </TableCell>
