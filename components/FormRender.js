import React from 'react';
import {formErrorStore} from "../ui/FormInput";
import {Button} from "@mui/material";
import {observer} from "mobx-react-lite";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {baseStore} from "../store/BaseStore";

export const FormRender = observer(({children, onSubmit, btnText, schema, model}) => {
    const _onSubmit = () => {
        validateForm(model, schema)
        if (onSubmit && formErrorStore.isValid()) onSubmit(model)
    }

    return (
        <form style={{maxWidth: 500, margin: '0 auto'}}>
            {children}
            <Button onClick={_onSubmit}
                    disabled={baseStore.loading}
                    variant={"outlined"}>{baseStore.loading ? 'Загрузка' : btnText }</Button>
            <ToastContainer
                position="top-center"
                autoClose={4000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </form>
    )
})

function validateForm(model, schema) {
    const {updateError, deleteError} = formErrorStore
    if (model && schema) {
        for (const key in schema) {
            if (schema[key].required) {
                if (!key in model || typeof model[key] === 'undefined') {
                    updateError(key, 'required', `Field ${key} is required`)
                } else if (key in model) {
                    if (typeof model[key] === 'string') {
                        if (model[key].length === 0) {
                            updateError(key, 'required', `Field ${key} is required`)
                        } else deleteError(key, 'required')
                    } else if (typeof model[key] === 'object') {
                        if (Object.keys(model[key]).length === 0) {
                            updateError(key, 'required', `Field ${key} is required`)
                        } else deleteError(key, 'required')
                    }
                }
            }

            if (schema[key].regex && key in model && typeof model[key] === 'string') {
                const re = schema[key].regex
                if (!re.test(model[key])) {
                    updateError(key, 'regex', `Field ${key} no validate`)
                } else deleteError(key, 'regex')
            }
        }
    }
}

export default FormRender;
