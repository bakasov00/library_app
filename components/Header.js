import React from 'react';
import Navbar from "./Navbar";
import Head from "next/head";

const Header = ({head, config}) => {
    return (
        <>
            <Head>
                <title>{head?.title || "Библиотека"}</title>
                <meta name="description" content="Author @ariet00"/>
            </Head>
            <Navbar/>
        </>
    );
};

export default Header;