import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Link from 'next/link'

const navItems = [
    {id: 1, link: '/', name: 'Главная'},
    {id: 3, link: '/author', name: 'Авторы'},
    {id: 2, link: '/book', name: 'Книги'}
]

const Navbar = () => {
    return (
        <Box sx={{flexGrow: 1, mb: 2}}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{flexGrow: 1}}>
                        <Link href={'/'}>
                            Dream Library
                        </Link>
                    </Typography>
                    {navItems.map((item,idx) => (
                        <span key={idx} style={{marginRight: 15}}>
                            <Link href={item.link}>
                            {item.name}
                        </Link>
                        </span>
                    ))}
                </Toolbar>
            </AppBar>
        </Box>
    );
}

export default Navbar;

