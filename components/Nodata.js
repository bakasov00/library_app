import React from 'react';

const Nodata = ({title, textAlign='center'}) => {
    return (
        <div style={{textAlign}}>
            {title || 'Нет данных'}
        </div>
    );
};

export default Nodata;