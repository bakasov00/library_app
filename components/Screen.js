import React from 'react';
import {Container} from "@mui/material";

const Screen = ({children}) => {
    return (
        <>
            <Container>
                {children}
            </Container>
        </>
    );
};

export default Screen;