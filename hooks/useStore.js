import {useContext} from "react";
import {StoreContext} from "../providers/RooProvider";

export function useStore() {
    const context = useContext(StoreContext);
    if (context === undefined) {
        throw new Error("useRootStore must be used within RootStoreProvider");
    }
    return context;
}


export function useBookStore() {
    const {bookStore} = useStore()
    return bookStore
}

export function useAuthorStore() {
    const {authorStore} = useStore()
    return authorStore
}
