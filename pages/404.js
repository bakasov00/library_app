import Link from 'next/link'

export default function FourOhFour() {
    return (
        <div style={{textAlign: 'center'}}>
            <h1>404 - Страница не найдена</h1>
            <br/>
            <Link href="/">На главную</Link>
        </div>
    )
}