import '../styles/globals.css'
import {RootProvider} from "../providers/RooProvider";
import React from "react";
import Header from "../components/Header";

function MyApp({Component, pageProps}) {
    return (
        <RootProvider hydrationData={pageProps.hydrationData}>
            <Header/>
            <Component {...pageProps} />
        </RootProvider>
    )
}

export default MyApp
