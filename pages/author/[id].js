import React from 'react';
import Screen from "../../components/Screen";
import {authorApi} from "../../api/author";
import {useAuthorStore} from "../../hooks/useStore";

const AuthorDetail = () => {
    const {item} = useAuthorStore()
    return (
        <Screen>
            <div><strong>Имя автора: </strong> {item.name} </div>
            <br/>
            <div><strong>Фамилия автора: </strong> {item.surname} </div>
        </Screen>
    );
};

export default AuthorDetail;

export async function getServerSideProps(context) {
    const {id} = context.params
    const data = await authorApi.get(id)
    console.log('data', data)
    if (!data?.item) {
        return {
            notFound: true,
        }
    }
    return {
        props: {
            hydrationData: {authorStore: {item: data?.item || {}}}
        },
    }
}