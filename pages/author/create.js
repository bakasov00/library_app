import React from 'react';
import Screen from "../../components/Screen";
import {Input} from "../../ui/FormInput";
import {useAuthorStore} from "../../hooks/useStore";
import FormRender from "../../components/FormRender";


const Create = () => {
    const authorStore = useAuthorStore()
    const {item, schema} = authorStore

    const handlerSubmit = async (data) => {
        await authorStore.createAuthor(data)
    }

    return (
        <Screen>
            <FormRender schema={schema} model={item} btnText={'Создать'} onSubmit={handlerSubmit} >
                <Input fullWidth label={'Имя'} model={item} name={'surname'} />
                <Input fullWidth label={'Фамилия'} model={item} name={'name'} />
            </FormRender>
        </Screen>
    );
};

export default Create;