import React from 'react';
import Screen from "../../components/Screen";
import {Input} from "../../ui/FormInput";
import {authorApi} from "../../api/author";
import {useAuthorStore} from "../../hooks/useStore";
import FormRender from "../../components/FormRender";

const Edit = () => {
    const authorStore = useAuthorStore()
    const {item, schema} = authorStore

    const handlerSubmit = async (data) => {
        await authorStore.editAuthor(data)
    }

    return (
        <Screen>
            <FormRender schema={schema} model={item} btnText={'Сохранить'} onSubmit={handlerSubmit}>
                <Input fullWidth label={'Имя'} model={item} name={'surname'}/>
                <Input fullWidth label={'Фамилия'} model={item} name={'name'}/>
            </FormRender>
        </Screen>
    );
};

export default Edit;

export async function getServerSideProps(context) {
    const {id} = context.query
    const data = await authorApi.get(id, true)
    return {
        props: {
            hydrationData: {authorStore: {item: data?.item || {} }}
        },
    }
}