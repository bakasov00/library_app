import Screen from "../../components/Screen";
import DataTable from "../../components/DataTable";
import {Button} from "@mui/material";
import Link from 'next/link'
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import DropDown from "../../components/DropDown";
import * as React from "react";
import {authorApi} from "../../api/author";
import {useAuthorStore} from "../../hooks/useStore";
import MenuItem from "@mui/material/MenuItem";
import {observer} from "mobx-react-lite";
import Nodata from "../../components/Nodata";


const Home = observer(() => {
    const authorStore = useAuthorStore()
    const {list, headTable} = authorStore

    async function handleDelete(id) {
        const ok = confirm('Удаляться все книги автора. Действительно хотите удалить?')
        if (ok) await authorStore.deleteAuthor(id)
    }


    return (
        <Screen>
            <Link href={'/author/create'}><Button variant={'outlined'}>Создать автора</Button></Link>
            {list.length !== 0 ? <DataTable headTable={headTable}>
                {list.map(author => (
                    <TableRow key={author.id}>
                        <TableCell align={"left"}><Link
                            href={`/author/${author.id}`}><a>{author?.surname} {author.name}</a></Link></TableCell>
                        <TableCell align={"right"}>{author?.name}</TableCell>
                        <TableCell align={"right"}>{author?.surname}</TableCell>
                        <TableCell align={"right"}>
                            <DropDown>
                                <MenuItem><Link href={`/author/${author.id}`}>Просмотр</Link></MenuItem>
                                <MenuItem><Link href={{
                                    pathname: '/author/edit',
                                    query: {id: author.id},
                                }}>Редактировать</Link></MenuItem>
                                <MenuItem onClick={() => handleDelete(author.id)}>Удалить</MenuItem>
                            </DropDown>
                        </TableCell>
                    </TableRow>
                ))}
            </DataTable> : <Nodata/>}
        </Screen>
    )
})

export default Home

export async function getServerSideProps() {
    const data = await authorApi.getAll()
    return {
        props: {
            hydrationData: {authorStore: {list: data?.list || []}}
        },
    }
}