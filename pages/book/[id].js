import React from 'react';
import Screen from "../../components/Screen";
import {useBookStore} from "../../hooks/useStore";
import {bookApi} from "../../api/book";

const BookDetail = () => {
    const {item} = useBookStore()

    return (
        <Screen>
            <div><strong>Название книги: </strong> {item.title} </div>
            <br/>
            <div><strong>Дата публикация: </strong> {item.createdAt} </div>
            <br/>
            <div><strong>Имя автора: </strong> {item.author?.name} </div>
            <br/>
            <div><strong>Фамилия автора: </strong> {item.author?.surname} </div>
        </Screen>
    );
};


export default BookDetail;

export async function getServerSideProps(context) {
    const {id} = context.params
    const data = await bookApi.get(id)

    if (!data?.item) {
        return {
            notFound: true
        }
    }

    return {
        props: {
            hydrationData: {bookStore: {item: data?.item || {}}}
        },
    }
}