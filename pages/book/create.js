import React from 'react';
import Screen from "../../components/Screen";
import { Input, InputSelect} from "../../ui/FormInput";
import {useAuthorStore, useBookStore} from "../../hooks/useStore";
import {authorApi} from "../../api/author";
import FormRender from "../../components/FormRender";


const Create = () => {
    const {list} = useAuthorStore()
    const bookStore = useBookStore()
    const {item, schema} = bookStore

    const handleSubmit = async (data) => {
        await bookStore.createBook(data)
    }

    return (
        <Screen>
            <FormRender schema={schema} onSubmit={handleSubmit} btnText={'Создать'} model={item}>
                <Input fullWidth label={'Название книги'} model={item} name={'title'}/>
                <InputSelect valueName={'fullName'} options={list} fullWidth label={'Выберите автора'} model={item} name={'author'}/>
            </FormRender>
        </Screen>
    );
};

export default Create;

export async function getServerSideProps() {
    const data = await authorApi.getAll()
    return {
        props: {
            hydrationData: {
                authorStore: {list: data?.list || []},
            }
        },
    }
}