import React from 'react';
import {Input, InputSelect} from "../../ui/FormInput";
import Screen from "../../components/Screen";
import {bookApi} from "../../api/book";
import {authorApi} from "../../api/author";
import {useAuthorStore, useBookStore} from "../../hooks/useStore";
import FormRender from "../../components/FormRender";

const Edit = () => {
    const {list} = useAuthorStore()
    const bookStore = useBookStore()
    const {item, schema} = bookStore

    const handlerSubmit = async (data) => {
        await bookStore.editBook(data)
    }

    return (
        <Screen>
            <FormRender schema={schema} model={item} btnText={'Сохранить'} onSubmit={handlerSubmit} >
                <Input fullWidth label={'Название книги'} model={item} name={'title'} />
                <InputSelect value={item.author}
                             fullWidth
                             options={list}
                             label={'Выберите автора'}
                             name={'author'}
                             valueName={'fullName'}
                             model={item}/>
            </FormRender>
        </Screen>
    );
};

export default Edit;

export async function getServerSideProps(context) {
    const {id} = context.query
    const book = await bookApi.get(id, true)
    const authors = await authorApi.getAll()
    return {
        props: {
            hydrationData: {
                authorStore: {list: authors?.list || []},
                bookStore: {item: book?.item || {}}
            }
        },
    }
}
