import Screen from "../../components/Screen";
import DataTable from "../../components/DataTable";
import {Button} from "@mui/material";
import TableRow from "@mui/material/TableRow";
import * as React from "react";
import TableCell from "@mui/material/TableCell";
import DropDown from "../../components/DropDown";
import Link from 'next/link'
import {useBookStore} from "../../hooks/useStore";
import {bookApi} from "../../api/book";
import MenuItem from "@mui/material/MenuItem";
import {observer} from "mobx-react-lite";
import Nodata from "../../components/Nodata";


const Home = observer(() => {
    const bookStore = useBookStore()
    const {list, headTable} = bookStore

    async function handlerDelete(id) {
        await bookStore.deleteBook(id)
    }

    return (
        <Screen>
            <Link href={'/book/create'}><Button variant={"outlined"}>Создать новую книгу</Button></Link>
            <br/>
            {list.length !== 0 ? <DataTable headTable={headTable}>
                {list.map(book => (
                    <>
                        <TableRow key={book.id}>
                            <TableCell align={"left"}><Link href={`/book/${book.id}`}>{book.title}</Link></TableCell>
                            <TableCell align={"right"}>{book.author?.name}</TableCell>
                            <TableCell align={"right"}>{book.author?.surname}</TableCell>
                            <TableCell align={"right"}>{book.createdAt}</TableCell>
                            <TableCell align={"right"}>
                                <DropDown>
                                    <MenuItem><Link href={`/book/${book.id}`}>Просмотр</Link></MenuItem>
                                    <MenuItem><Link href={{
                                        pathname: '/book/edit',
                                        query: {id: book.id},
                                    }}>Редактировать</Link></MenuItem>
                                    <MenuItem onClick={() => handlerDelete(book.id)}>Удалить</MenuItem>
                                </DropDown>
                            </TableCell>
                        </TableRow>
                    </>
                ))}
            </DataTable> : <Nodata/>}
        </Screen>
    )
})

export default Home

export async function getServerSideProps() {
    const data = await bookApi.getAll()
    return {
        props: {
            hydrationData: {bookStore: {list: data?.list || []}}
        },
    }
}