import Link from 'next/link'
import styles from '../styles/Home.module.css'
import Screen from "../components/Screen";

export default function Home() {
    return (
        <Screen>
            <h1 className={styles.title}>
                Dream Library
            </h1>
            <div className={styles.grid}>
                <Link href="/author">
                    <a className={styles.card}>
                        <h2>Авторы &rarr;</h2>
                        Самые лучшие авторы со всего мира.
                    </a>
                </Link>
                <Link href="/book" className={styles.card}>
                    <a className={styles.card}>
                        <h2>Книги &rarr;</h2>
                        Разные книги со всего мира для всех.
                    </a>
                </Link>
            </div>
        </Screen>
    )
}
