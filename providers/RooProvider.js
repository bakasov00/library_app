import {enableStaticRendering} from "mobx-react-lite";
import React, {createContext} from "react";
import {RootStore} from "../store";

enableStaticRendering(typeof window === "undefined");

let store
export const StoreContext = createContext(undefined);

export function RootProvider({children, hydrationData}) {
    const store = initializeStore(hydrationData);
    return (
        <StoreContext.Provider value={store}>{children}</StoreContext.Provider>
    );
}

function initializeStore(initialData) {
    const _store = store ?? new RootStore();

    if (initialData) {
        _store.hydrate(initialData);
    }
    if (typeof window === "undefined") return _store;
    if (!store) store = _store;

    return _store;
}