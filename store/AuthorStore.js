import {action, makeObservable} from 'mobx'
import {BaseStore} from "./BaseStore";
import {authorApi} from "../api/author";

export class AuthorStore extends BaseStore {
    headTable = [
        "ФИО",
        "Фамилия",
        "Имя",
        ''
    ]
    schema = {
        surname: {required: true},
        name: {required: true},
    };

    @action
    async deleteAuthor(id) {
        const data = await authorApi.delete(id)
        if (data.message) return this.notify(data.message, 'error')
        super.delete(data.id);
    }

    @action
    async editAuthor(item) {
        const data = await authorApi.edit(item.id, item)
        if (data.message) return this.notify(data.message, 'error')
        this.notify('Данные успешно изменены', 'success')
        super.post(data.item);
    }

    @action
    async createAuthor(item) {
        const data = await authorApi.create(item)
        if (data.message)  {
            this.notify(data.message, 'error')
            return
        }
        this.notify('Данные успешно сохранены', 'success')
        super.post(data.item);
    }

    constructor() {
        super()
        makeObservable(this)
    }
}
