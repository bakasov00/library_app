import {action, makeObservable, observable} from "mobx";
import {toast} from "react-toastify";

export class BaseStore {
    constructor() {
        makeObservable(this)
    }
    @observable loading = false
    @observable list = []
    @observable item = {}

    @action postData (list) {
        this.list = [...this.list, ...list]
    }

    @action post (item) {
        this.item = item
    }

    @action delete (id) {
        this.list = this.list.filter(f => f.id !== id)
    }

    notify (message, type = 'info') {
        return toast[type](message, {
            position: "top-center",
            autoClose: 4000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
    @action hydrate = (data) => {
        if (!data) return
        this.list = data.list ? data.list : []
        this.item = data.item ? data.item : {}
    }
}

export const baseStore = new BaseStore()
