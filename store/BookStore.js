import {action, makeObservable} from 'mobx'
import {BaseStore} from "./BaseStore";
import {bookApi} from "../api/book";

export class BookStore extends BaseStore {
    headTable = [
        'Название книги',
        'Фамилия автора',
        'Имя автора',
        'Дата публикация',
        ''
    ]
    schema = {
        title: {required: true},
        author: {required: true}
    }

    @action
    async deleteBook(id) {
        const data = await bookApi.delete(id)
        if (data.message) return this.notify(data.message, 'error')
        super.delete(data.id);
    }

    @action
    async editBook(item) {
        const data = await bookApi.edit(item.id, item)
        if (data.message) return this.notify(data.message, 'error')
        this.notify('Данные успешно изменены', 'success')
        super.post(data.item);
    }

    @action
    async createBook(item) {
        const data = await bookApi.create(item)
        if (data.message) {
            this.notify(data.message, 'error')
            return
        }
        this.notify('Данные успешно сохранены', 'success')
        super.post(data.item);
    }

    constructor() {
        super()
        makeObservable(this)
    }
}
