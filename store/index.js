import {BaseStore} from "./BaseStore";
import {BookStore} from "./BookStore";
import {action} from "mobx";
import {AuthorStore} from "./AuthorStore";


export class RootStore {
    constructor() {
        this.baseStore = new BaseStore(this);
        this.bookStore = new BookStore(this);
        this.authorStore = new AuthorStore(this);
    }

    @action hydrate(data) {
        if (data.bookStore) {
            this.bookStore.hydrate(data.bookStore);
        }
        if (data.authorStore) {
            this.authorStore.hydrate(data.authorStore);
        }
    }
}
