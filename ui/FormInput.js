import React, {createContext, useEffect, useState} from 'react';
import {FormHelperText, InputLabel, MenuItem, Select, TextField} from '@mui/material'
import {action, makeAutoObservable, observable} from 'mobx'
import {observer} from 'mobx-react-lite'
import Box from "@mui/material/Box";
import Nodata from "../components/Nodata";

export const FromContext = createContext(undefined)

class FormError {
    @observable errors = {}

    constructor() {
        makeAutoObservable(this)
    }

    isValid() {
        return Object.keys(this.errors).length === 0
    }

    @action updateError = (key, type, message) => {
        this.errors = {...this.errors, [key]: {[type]: message}}
    }

    @action deleteError = (key, type) => {
        if (key in this.errors) {
            if (type in this.errors[key]) {
                const obj = this.errors[key]
                delete obj[type]
            }
            if (Object.keys(this.errors[key]).length === 0) delete this.errors[key]
        }
    }
}

export const formErrorStore = new FormError()

export const Input = observer(({model, name, label, onChange, type, options, ...rest}) => {
    const [value, setValue] = useState(getValue())
    const error = formErrorStore.errors[name]

    function getValue() {
        if (model && name) {
            return model[name] ? model[name] : ''
        }
        return ''
    }

    const handleChange = (e) => {
        const _value = e.target.value
        if (onChange) onChange(_value)
        setValue(_value)
    }

    useEffect(() => {
        if (model && name) {
            model[name] = value
        }
    }, [value])

    return (
        <div>
            <InputLabel>{label}</InputLabel>
            <TextField
                style={{marginBottom: 10}}
                variant={"outlined"}
                value={type === 'file' ? '' : value}
                onChange={handleChange}
                name={name}
                error={error && Object.keys(error).length}
                type={type || 'text'}
                {...rest}
                helperText={error && Object.values(error)[0]}
            />
        </div>
    );
});


export const InputSelect = observer(({
                                         valueName = 'name', valueCode = 'id',
                                         value, options, defaultValue, model, name, label, ...rest
                                     }) => {
    const [select, setSelect] = useState(getValue())
    const error = formErrorStore.errors[name]

    useEffect(() => {
        if (model && name) model[name] = select
    }, [select])

    const handleChange = (e) => {
        const _value = e.target.value
        const select = options.find(f => f[valueCode] === _value)
        setSelect(select)
    }

    function getValue() {
        let _val = value ? value : Array.isArray(options) && options.length > 0 && options[0]
        if (_val && typeof _val === 'object') return _val
        return null
    }

    return (
        <Box style={{marginBottom: 10, width: '100%'}}>
            <InputLabel>{label || 'Выберите'}</InputLabel>
            <Select
                value={select && select[valueCode]}
                onChange={handleChange}
                placeholder={'Выберите'}
                error={error && Object.keys(error).length}
                {...rest}
            >
                {options.length !== 0 ?
                    options.map(option => (
                        <MenuItem value={option[valueCode]}>{option[valueName]}</MenuItem>
                    ))
                    :
                    <Nodata/>
                }
            </Select>
            <FormHelperText style={{marginLeft: 10, color: 'red'}}>{error && Object.values(error)[0]}</FormHelperText>
        </Box>
    )
});
